from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.core.mail import send_mail
from sports.models import *
from django.core.paginator import Paginator

# Create your views here.

class HomePageView(TemplateView):
    template_name = "sports/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        articles = Article.objects.all()
        context['latest_articles'] = articles[:4]

        return context


class HowItWorksPageView(TemplateView):
    template_name = "sports/how-it-works.html"

class BuildSystemsPageView(TemplateView):
    template_name = "sports/build-systems.html"

class ExploreSystemsPageView(TemplateView):
    template_name = "sports/explore-systems.html"

class PricingPageView(TemplateView):
    template_name = "sports/pricing.html"

class StrategyPageView(TemplateView):
    template_name = "sports/strategy.html"


class BlogPageView(TemplateView):
    template_name = "sports/blog.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        articles = Article.objects.all()
        context['latest_articles'] = articles[:4]
        if self.request.GET.get('s'):
            s = self.request.GET.get('s')
            articles = Article.objects.filter(title__contains=s)

        paginator = Paginator(articles, 4)  # Show 4 articles per page
        page = self.request.GET.get('page')
        context['articles'] = paginator.get_page(page)


        return context

def ContactPageView(request):
    if request.method == 'POST':
        mail = 'mail@gmail.com'  # E-mail of the person sending it (It is configured in setting.py)
        email = request.POST.get('g6338-email')  # Mail that the visitor of the page enters
        message = request.POST.get('g6338-comment')
        name = request.POST.get('g6338-name')
        tittle = request.POST.get('g6338-tittle')
        receiver = ['mail2@gmail.com']  # Mail to which the message is sent
        message_body = name + "\n" + email + "\n\n\n" + message
        # Django's own function for sending mail
        send_mail(
            tittle,
            message_body,
            mail,
            receiver,
            fail_silently=False,
        )

    return render(request, "sports/contact.html")

class ArticleDetailView(TemplateView):
    template_name = "sports/article_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs['pk']
        articles = Article.objects.all()
        context['categories'] = Category.objects.all()
        context['article'] = Article.objects.get(pk=pk)

        next_article_id = False
        try:
            next_article_id = Article.objects.filter(id__gt=pk).order_by("id")[0:1].get().id
        except:
            pass

        previous_article_id = False
        try:
            previous_article_id = Article.objects.filter(id__lt=pk).order_by("-id")[0:1].get().id
        except:
            pass

        context['next_article_id'] = next_article_id
        context['previous_article_id'] = previous_article_id
        category = context['article'].category
        context['related_articles'] = Article.objects.filter(category=category)[:3]
        context['latest_articles'] = articles[:4]

        return context


class CategoryPageView(TemplateView):
    template_name = "sports/blog.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category = int(self.kwargs['pk'])
        context['categories'] = Category.objects.all()
        articles = Article.objects.filter(category=category)
        paginator = Paginator(articles, 4)  # Show 4 articles per page
        page = self.request.GET.get('page')
        context['articles'] = paginator.get_page(page)
        context['latest_articles'] = articles[:4]

        return context