from django.contrib import admin
from sports.models import *

# Register your models here.
admin.site.register(Category)
admin.site.register(Article)