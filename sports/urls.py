from django.urls import path
from sports.views import *

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('how-it-works', HowItWorksPageView.as_view(), name='how-it-works'),
    path('build-systems', BuildSystemsPageView.as_view(), name='build-systems'),
    path('explore-systems', ExploreSystemsPageView.as_view(), name='explore-systems'),
    path('pricing', PricingPageView.as_view(), name='pricing'),
    path('strategy', StrategyPageView.as_view(), name='strategy'),
    path('blog', BlogPageView.as_view(), name='blog'),
    path('article/<int:pk>', ArticleDetailView.as_view(), name='article'),
    path('category/<int:pk>', CategoryPageView.as_view(), name='category'),
    path('contact', ContactPageView, name='contact'),
]