from django.db import models

# Create your models here.
class Category(models.Model):
    name = models.CharField('Name', max_length=128, blank=False, null=False, unique=True)

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name

class Article(models.Model):
    title = models.CharField('Title', max_length=128, blank=False, null=False)
    author = models.CharField('Author', max_length=64, blank=False, null=False)
    date = models.DateTimeField('Date', blank=False, null=False)
    description = models.TextField('Description', max_length=4096, blank=False, null=False)
    image = models.ImageField('Image', upload_to="article_images", blank=False, null=False)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)

    def __str__(self):
        return self.title
